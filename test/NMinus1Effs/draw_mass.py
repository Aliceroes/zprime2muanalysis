#!/usr/bin/env python

# (py draw.py >! plots/nminus1effs/out.draw) && tlp plots/nminus1effs

from pprint import pprint
import sys, os
from array import array
from SUSYBSMAnalysis.Zprime2muAnalysis.roottools import *
set_zp2mu_style()
ROOT.gStyle.SetPadTopMargin(0.02)
ROOT.gStyle.SetPadRightMargin(0.02)
ROOT.gStyle.SetTitleX(0.12)
#ROOT.gStyle.SetTitleH(0.07)
ROOT.TH1.AddDirectory(0)

from nm1entry import *

print "cartella: ", cartella

# AliceNoNo = 'NoNo03had010'
AliceNoNo = 'NoNo03all025'
# NOTA: in NoNo cambiare il path dei DY in def make_fn(self, name) !
# AliceNoNo = 'NoNo'

if AliceNoNo=='NoNo03had010':
    outfile = ROOT.TFile("IsoStudies_vsMass/pfIso03had010.root", "recreate");
    AliceColor = ROOT.kBlue+2
    # pretty_AliceNoNo = 'pfIso(#DeltaR=0.3)<0.10'
    # pretty_AliceNoNo2 = 'charged hadrons only'
elif AliceNoNo=='NoNo03all025':
    outfile = ROOT.TFile("IsoStudies_vsMass/pfIso03all025.root", "recreate");
    AliceColor = ROOT.kRed+1
    # pretty_AliceNoNo = 'pfIso(#DeltaR=0.3)<0.25'
    # pretty_AliceNoNo2 = ''
elif AliceNoNo=='NoNo':
    outfile = ROOT.TFile("IsoStudies_vsMass/TrackerIso.root", "recreate");
    AliceColor = ROOT.kGreen+2
else:
    outfile = ROOT.TFile("whargl_mass.root","recreate")


iarp=0
mcN=0
drawDY = True
do_tight = 'tight' in sys.argv
#psn = 'plots/nminus1effs'
psn = 'plots/IsoStudies/' + cartella + '/mass/'
#psn = 'plots/paper2016/mass_onlyDY'
# 'plots' = '/afs/cern.ch/work/c/cschnaib/NMinus1Effs/plots/TAG/'
if do_tight:
    psn += '_tight'
ps = plot_saver(psn, size=(600,600), log=False, pdf=True)
ps.c.SetBottomMargin(0.2)


if do_tight:
    nminus1s = [
        #'TiPt',
        # 'TiDB',
        # 'TiGlbChi2',
        'TiIso',
        # 'TiTkLayers',
        # 'TiPxHits',
        # 'TiMuHits',
        # 'TiMuMatch',
        ]
else:
    nminus1s = [
        # 'NoPt',
        # 'NoDB',
        'NoIso',
        # 'NoTkLayers',
        # 'NoPxHits',
        # 'NoMuHits',
        # 'NoMuMatch',
        # 'NoVtxProb',
        # 'NoB2B',
        # 'NoDptPt',
        #         #'NoCosm',
        # 'NoTrgMtch',
        ]


class nm1entry:
    def __init__(self, sample, is_data, lumi):
        print 'ESEGUO NM1ENTRY'
        if type(sample) == str:
            print "filedati"
            self.name = sample
            # self.fn = '/afs/cern.ch/work/a/aalfonsi/public/nminus1_histos_pfiso/ana_nminus1_%s.root'%sample
            self.fn = '/afs/cern.ch/work/a/aalfonsi/public/nminus1_histos_data/Run2016MuonsOnly/ana_nminus1_%s.root'%sample
            print sample
            #self.fn = self.make_fn(sample) if is_data else None
            self.lumi = lumi if is_data else None
            self.is_data = is_data
            print is_data
        else:
            print "sample no str (MonteCarlo)";
            self.name = sample.name
            self.fn = self.make_fn(self.name)
            self.partial_weight = sample.partial_weight
            self.is_data = is_data
        self.prepare_histos()

    def make_fn(self, name):
        # DY per TrackerIso<0.10 (standard):
        # return '/afs/cern.ch/work/a/aalfonsi/public/IsoStudiesDY/ana_nminus1_%s.root' % name
        # DY per pfIso_all<0.25 e pfIso_had<0.10:
        return '/afs/cern.ch/work/a/aalfonsi/public/nminus1_histos_pfIsoEffStudies/ana_nminus1_%s.root' % name

    def prepare_histos(self):
        print 'Eseguo PREPAREHISTOS'
        self.histos = {}
        if self.fn is not None:
            f = ROOT.TFile(self.fn)
            # CAMBIARE NoNo con la cartella nel rootfile in cui si hanno i NoNo giusti per lo studio di pfIso scelta
            # for nminus1 in nminus1s + ['NoNo']:
            # for nminus1 in nminus1s + ['NoNo03all025']:
            for nminus1 in nminus1s + [AliceNoNo]:
                print nminus1
                self.histos[nminus1] = f.Get(nminus1).Get(cartella).Clone()#DileptonMass
                '''
                if nminus1=='NoVtxProb':
                    #self.histos[nminus1] = f.Get(nminus1).Get('DileptonMass').Clone()
                    self.histos[nminus1] = f.Get(nminus1).Get(cartella).Clone()
                else:
                    self.histos[nminus1] = f.Get(nminus1).Get(cartella).Clone()#DileptonMass
                '''

    def prepare_histos_sum(self, samples, lumi):
        self.histos = {}
        for nminus1 in nminus1s + ['NoNo']:
            hs = []
            #print '%20s%20s%21s%20s%20s' % ('cut', 'sampe name', 'partial weight', 'scale(ref)','scale(lumi)')
            for sample in samples:
                f = ROOT.TFile(self.make_fn(sample.name))
                if nminus1 == 'NoVtxProb':
                    #h = f.Get(nminus1).Get('DileptonMass').Clone()
                    h = f.Get(nminus1).Get(cartella).Clone()
                else:
                    h = f.Get(nminus1).Get(cartella).Clone()
                #print '%20s%20s%20.15f%20f%20f' % (nminus1, sample.name, sample.partial_weight, refN/refXS, lumiBCD)
                # partial_weight = cross_section * k_factor / Nevents
                if lumi>0:
                    # scale to luminosity for comparision of single dataset to MC
                    h.Scale(sample.partial_weight * lumi)
                    #print nminus1, sample.name, sample.partial_weight*lumi
                    #print '%20s%20s%20.10f%20f' % (nminus1, sample.name, sample.partial_weight, lumi)
                if lumi<0:
                    # scale to reference cross section/Nevents for comparision of multiple datasets to MC
                    h.Scale(sample.partial_weight * refN / refXS)
                    #print nminus1, sample.name, sample.partial_weight*refN/refXS
                    #print '%20s%20s%20.10f%20f' % (nminus1, sample.name, sample.partial_weight, refN/refXS)
                hs.append(h)
            hsum = hs[0].Clone()
            for h in hs[1:]:
                hsum.Add(h)
            self.histos[nminus1] = hsum

#data, lumi = nm1entry('data', True), 242.8 # lumi in pb
nolumi = -1
#lumiB = 50.7
lumi = 7600.691 #8192.287 #13066.191 #6293.188#4079.344#2231.20#1#2260.881 #2619.44
#lumiD = 2572.19
#lumiBCD = 2660.14
#lumiBCD = 2800.
#dataB = nm1entry('dataB', True, lumiB)#lumiB )
data = nm1entry('data', True, lumi)#lumiCD )
#dataBCD = nm1entry('dataBCD', True, lumiBCD)#lumiCD )
#dataD = nm1entry('dataD', True, lumiD )
#mcsum_lumi = nm1entry('mcsum_lumi',False,lumiBCD)
#mcsum_ref = nm1entry('mcsum_ref',False,nolumi)
#DYmc = nm1entry('DYmc',False,lumiBCD)
#nonDYmc = nm1entry('nonDYmc',False,lumiBCD)
#wjets = nm1entry('wjets',False,lumiBCD)

from SUSYBSMAnalysis.Zprime2muAnalysis.MCSamples import *

#raw_samples = [dy50to120,dy120to200,dy200to400,dy400to800,dy800to1400,dy1400to2300,dy2300to3500,dy3500to4500,dy4500to6000,ttbar_pow,WWinclusive,ZZ,WZ,tW,Wantitop]
raw_samples = [dy50to120,dy120to200,dy200to400,dy400to800,dy800to1400,dy1400to2300,dy2300to3500,dy3500to4500,dy4500to6000]#,

# raw_samples = [DY1400to2300Powheg]#,

#use_samples = [tW,Wantitop,WWinclusive,WZ,ZZ,ttbar_lep,dy50to120,dy120to200,dy200to400,dy400to800,dy800to1400,dy1400to2300,dy2300to3500,dy3500to4500,dy4500to6000]
use_samples = [dy50to120,dy120to200,dy200to400,dy400to800,dy800to1400,dy1400to2300,dy2300to3500,dy3500to4500,dy4500to6000]#,ttbar_lep, dyInclusive50print lumi

# use_samples = [DY1400to2300Powheg]#,ttbar_lep, dyInclusive50print lumi



#refXS = dy50to120_s.cross_section
#refN = dy50to120_s.nevents

# All MC samples
# lumi
#mcsum_lumi.prepare_histos_sum(use_samples,lumiBCD)
mc_samples = [nm1entry(sample,False,lumi) for sample in use_samples]
for mc_sample in mc_samples:
    exec '%s = mc_sample' % mc_sample.name

#dy = [nm1entry(sample,False,lumiBCD) for sample in dy_samples]
#for dy_sample in dy_samples:
#    exec '%s = dy_sample' % dy_sample.name
#noDY = [nm1entry(sample,False,lumiBCD) for sample in nonDY_samples]
#for nonDY_sample in nonDY_samples:
#    exec '%s = nonDY_sample' % nonDY_sample.name
#qcd = [nm1entry(sample,False,lumiBCD) for sample in QCD_samples]
#for QCD_sample in QCD_samples:
#    exec '%s = QCD_sample' % QCD_sample.name
#diboson = [nm1entry(sample,False,lumiBCD) for sample in diboson_samples]
#stop = [nm1entry(sample,False,lumiBCD) for sample in stop_samples]
#ttbar = [nm1entry(sample,False,lumiBCD) for sample in ttbar_samples]
#noQCD = [nm1entry(sample,False,lumiBCD) for sample in noQCD_samples]
#wjets = [nm1entry(sample,False,lumiBCD) for sample in wjets_samples]
#noQCDwjets = [nm1entry(sample,False,lumiBCD) for sample in noQCDwjets_samples]
#EWKnoDY = [nm1entry(sample,False,lumiBCD) for sample in EWKnoDY_samples]
#QCDwjets = [nm1entry(sample,False,lumiBCD) for sample in QCDwjets_samples]
#Zpsi5000 = [nm1entry(sample,False,lumiBCD) for sample in zpsi5000_samples]
#EWKnoDYwjets = [nm1entry(sample,False,lumiBCD) for sample in EWKnoDYwjets_samples]
#

# ref
#mcsum_ref.prepare_histos_sum(use_samples, nolumi)
#mc_samples_ref = [nm1entry(sample,False,nolumi) for sample in use_samples]
#for mc_sample in mc_samples_ref:
#    exec '%s = mc_sample' % mc_sample.name

#DYmc.prepare_histos_sum(DYmc_list,lumiBCD)
#nonDYmc.prepare_histos_sum(nonDYmc_list,lumiBCD)

#bin_width = 20
#maxX = 2500
#minX = 60
#nBins = (maxX-minX)/bin_width
#mass_range = []
#for i in range(3,nBins):
#    ibin = i*bin_width
#    mass_range.append(ibin)
#print mass_range
#mass_range = [60,120,180,240,320,500,1000,2500]
mass_range = [50,120,200,400,800,1400,2300,3500,4500,6000] #4500,6000
to_use = {
    #  MC ONLY
    'NoPt':[mc_samples],
    'NoDB':[mc_samples],
    'NoIso':[mc_samples],
    'NoTkLayers':[mc_samples],
    'NoPxHits':[mc_samples],
    'NoMuHits':[mc_samples],
    'NoMuMatch':[mc_samples],
    'NoVtxProb':[mc_samples],
    'NoB2B':[mc_samples],
    'NoDptPt':[mc_samples],
    'NoTrgMtch':[mc_samples],
    # WITH DATA
    # 'NoPt':[mc_samples,data],
    # 'NoDB':[mc_samples,data],
    # 'NoIso':[mc_samples,data],
    # 'NoTkLayers':[mc_samples,data],
    # 'NoPxHits':[mc_samples,data],
    # 'NoMuHits':[mc_samples,data],
    # 'NoMuMatch':[mc_samples,data],
    # 'NoVtxProb':[mc_samples,data],
    # 'NoB2B':[mc_samples,data],
    # 'NoDptPt':[mc_samples,data],
    # 'NoTrgMtch':[mc_samples,data],
    }


yrange = {
#   'sample':    (ymin,ymax),
    'NoPt':      (0.8,1.01),
    'NoDB':      (0.8,1.01),
    'NoIso':     (0.8,1.01),
    'NoTkLayers':(0.8,1.01),
    'NoPxHits':  (0.8,1.01),
    'NoMuHits':  (0.8,1.01),
    'NoMuMatch': (0.8,1.01),
    'NoVtxProb': (0.8,1.01),
    'NoB2B':     (0.8,1.01),
    'NoDptPt':   (0.8,1.01),
    'NoTrgMtch': (0.8,1.01),
    }
#global_ymin = 0.
global_ymin = 0.9

def table_wald(entry,nminus1, mass_range):
    print entry.name, nminus1
    hnum = entry.histos['NoNo']
    hden = entry.histos[nminus1]
    #print '%20s%27s%23s%20s%16s%25s%26s' % ('cut', 'mass range','numerator', 'denominator', 'efficiency', '- 68% CL-CP +','68% CL-Wald')
    for mbin in range(len(mass_range)):
        if mbin == (len(mass_range)-1): break
        mlow = mass_range[mbin]
        mhigh = mass_range[mbin+1]
        num = get_integral(hnum, mlow, mhigh, integral_only=True, include_last_bin=False)
        den = get_integral(hden, mlow, mhigh, integral_only=True, include_last_bin=False)
        pcp,lcp,hcp = clopper_pearson(num, den)
        if num==0 and den==0:
            eff = 0
            errw = 0
        else:
            eff = num/den
            if (eff*(1-eff))<0:
                print "what is this"
                print 'NM1, sample, mlow, mhigh, num, den'
                print nminus1, entry.name, mlow, mhigh, num, den
                eff = 1
                errw = 0
            else:
                errw = (eff*(1-eff)/den)**0.5
        if 'data' not in entry.name:
            num = num*entry.partial_weight*lumi
            den = den*entry.partial_weight*lumi
        #print '%20s%15i%15i%20f%20f%15f%15f%15f%23f'     % (nminus1, mlow, mhigh, num, den, eff, eff-lcp, hcp-eff,        errw)
        #print '%20s%15i%15i%20f%20f%15f%15f%15f%15f%16f' % (nminus1, mlow, mhigh, num, den, eff, lcp,     hcp,     eff-errw, eff+errw)
        print ' '
    print '---------------------------------------------'

def table(entry,nminus1, mass_range):
    print entry.name
    hnum = entry.histos['NoNo']
    hden = entry.histos[nminus1]
    print '%20s%27s%23s%20s%20s%22s' % ('cut', 'mass range', 'numerator', 'denominator', 'efficiency', '68% CL')
    for mbin in range(len(mass_range)):
        if mbin == (len(mass_range)-1): break
        mlow = mass_range[mbin]
        mhigh = mass_range[mbin+1]
        num = get_integral(hnum, mlow, mhigh, integral_only=True, include_last_bin=False)
        den = get_integral(hden, mlow, mhigh, integral_only=True, include_last_bin=False)
        e,l,h = clopper_pearson(num, den)
        print '%20s%15i%15i%20f%20f%20f%15f%15f' % (nminus1, mlow, mhigh, num, den, p_hat, p_hat_e, p_hat_e)

ROOT.gStyle.SetTitleX(0.25)
ROOT.gStyle.SetTitleY(0.50)

for nminus1 in nminus1s:
    pretty_name = pretty[nminus1]
    print "nminuses:"
    print nminus1, pretty_name
    print "end"
    lg = ROOT.TLegend(0.25, 0.21, 0.91, 0.44)
    lg.SetTextSize(0.03)
    lg.SetFillColor(0)
    lg.SetBorderSize(1)

    same = 'A'
    effs = []

    for entry in to_use[nminus1]: #,mass_range

        #table(entry,nminus1, mass_range)

        l = len(mass_range)-1
        nminus1_num = ROOT.TH1F('num', '', l, array('f',mass_range))
        nminus1_den = ROOT.TH1F('den', '', l, array('f',mass_range))

        if not isinstance(entry,(list,tuple)) and 'data' in entry.name:
            print "data in entry name"
#         	if 'data' in entry.name:
#         		continue
            #table_wald(entry,nminus1,mass_range)
            color, fill = styles[entry.name]
            # hnum = entry.histos['NoNo03all025']
            hnum = entry.histos[AliceNoNo]
            # hnum = entry.histos['NoNo']
            hden = entry.histos[nminus1]
            for mbin in range(len(mass_range)):
                if mbin == (len(mass_range)-1): continue
                mlow = mass_range[mbin]
                mhigh = mass_range[mbin+1]
                num = get_integral(hnum, mlow, mhigh, integral_only=True, include_last_bin=False)
                den = get_integral(hden, mlow, mhigh, integral_only=True, include_last_bin=False)
                nminus1_num.SetBinContent(mbin+1, num)
                nminus1_den.SetBinContent(mbin+1, den)
            eff,p,epl,eph = binomial_divide(nminus1_num, nminus1_den)
            print "NM1, sample, mlow, mhigh, num, den"
            print nminus1, data.name, mlow, mhigh, num, den
        else:
            print "data not in entry name"
#             for a,mc in enumerate(entry):
#                 table_wald(mc,nminus1,mass_range)
            p_hats = []
            errsW = []
            x = []
            ex = []
            for mbin in range(len(mass_range)):
                if mbin == (len(mass_range)-1): continue
                numTot = 0
                denTot = 0
                err2sum = 0
                numsW = []
                densW = []
                err2s = []
                for i,mc in enumerate(entry):
                    # hnum = mc.histos['NoNo']
                    # hnum = mc.histos['NoNo03all025']
                    hnum = mc.histos[AliceNoNo]
                    hden = mc.histos[nminus1]
                    mlow = mass_range[mbin]
                    mhigh = mass_range[mbin+1]
                    numInt = get_integral(hnum, mlow, mhigh, integral_only=True, include_last_bin=False)
                    denInt = get_integral(hden, mlow, mhigh, integral_only=True, include_last_bin=False)
                    # ALICE: CONTROLLA MLOW E MHIGH COME SO DEFINITE
                    # numInt = get_integral(hnum, 1400, 2300, integral_only=True, include_last_bin=False)
                    # denInt = get_integral(hden, 1400, 2300, integral_only=True, include_last_bin=False)
                    # numInt = 1.
                    # denInt = 1.2
                    # print "numInt: ", numInt, "denInt: ", denInt
                    if numInt<=denInt and denInt>0:
                        p_hat_mc = float(numInt)/denInt
                        err2 = p_hat_mc*(1-p_hat_mc)/denInt
                    elif numInt>denInt:
                        # print "numInt>denInt"
                        # print "NM1, sample, mlow, mhigh, num, den"
                        # print nminus1, mc.name, mlow, mhigh, numInt, denInt
                        print
                        p_hat_mc = 1
                        err2 = 0
                    else:
                        if numInt!=0 or denInt!=0:
                            print "ELSE"
                            # print "NM1, sample, mlow, mhigh, num, den"
                            # print nminus1, mc.name, mlow, mhigh, numInt, denInt
                        p_hat_mc = 0
                        err2 = 0
                    numTot = numTot + numInt*mc.partial_weight
#                    print '%s ----- %s ---- %s' % (denTot, denInt, mc.partial_weight)
                    denTot = denTot + denInt*mc.partial_weight
                    numsW.append(numInt*mc.partial_weight)
                    densW.append(denInt*mc.partial_weight)
                    err2s.append(err2)
                p_hat = float(numTot)/denTot
                err2sum = sum( ((m/denTot)**2 * e2) for m,e2 in zip(densW,err2s))
                if err2sum<0:
                    err2sum = 0
                elif p_hat==0:
                    err2sum = 0
                p_hats.append(p_hat)
                errsW.append(err2sum**0.5)
                x.append(nminus1_num.GetXaxis().GetBinCenter(mbin+1))
                ex.append(nminus1_num.GetXaxis().GetBinWidth(mbin+1)/2)
            eff = ROOT.TGraphAsymmErrors(len(x), *[array('d',obj) for obj in (x,p_hats,ex,ex,errsW,errsW)])
        eff.SetTitle(pretty_name)
        ymin, ymax = yrange[nminus1]
        eff.GetYaxis().SetRangeUser(global_ymin if global_ymin is not None else ymin, ymax)
        eff.GetXaxis().SetTitle('m(#mu#mu) [GeV]')
        eff.GetYaxis().SetLabelSize(0.027)
        eff.GetYaxis().SetTitle('n-1 efficiency')
        if not isinstance(entry,(list,tuple)) and 'data' in entry.name:
            draw = 'P'
            eff.SetLineColor(color)
            eff.SetMarkerStyle(20)
            eff.SetMarkerSize(1.05)
            eff.SetMarkerColor(color)
            #lg.AddEntry(eff, pretty.get(entry.name, entry.name) % (lumi/1000.), 'LP')
            lg.AddEntry(eff, pretty.get(entry.name, entry.name) % (entry.lumi/1000.), 'LP')
        else:
            # if AliceNoNo=='NoNo03had010':
            #     print "\n\nECCO HNUM\n\n\n"
            #     draw = '2'
            #     eff.SetLineColor(ROOT.kRed+2)
            #     eff.SetFillColor(ROOT.kRed+2)
            #     eff.SetFillStyle(1001)
            #     lg.AddEntry(eff,'DY MC 80X','LF')
            if len(entry)==9:
                draw = '2'
                eff.SetLineColor(AliceColor)
                eff.SetFillColor(AliceColor)
                eff.SetFillStyle(1001)
                lg.AddEntry(eff,'DY MC 80X','LF')
            elif len(entry)==15:
                draw = '2'
                eff.SetLineColor(ROOT.kOrange+2)
                eff.SetFillColor(ROOT.kOrange+2)
                eff.SetFillStyle(1001)
                lg.AddEntry(eff,'EWK MC','LF')
            elif len(entry)==1:
                draw = '2'
                eff.SetLineColor(ROOT.kYellow+1)
                eff.SetFillColor(ROOT.kYellow+1)
                eff.SetFillStyle(1001)
                lg.AddEntry(eff,'Z\'_{#psi} (5 TeV)','LF')
            elif len(entry)==10:
                draw = '2'
                eff.SetLineColor(ROOT.kViolet)
                eff.SetFillColor(ROOT.kViolet)
                eff.SetFillStyle(1001)
                lg.AddEntry(eff,'QCD MC','LF')
            elif len(entry)==11:
                draw = '2'
                eff.SetLineColor(ROOT.kBlue+2)
                eff.SetFillColor(ROOT.kBlue+2)
                eff.SetFillStyle(1001)
                lg.AddEntry(eff,'QCD + W+jets MC','LF')
            elif len(entry)==7:
                draw = '2'
                eff.SetLineColor(ROOT.kRed+2)
                eff.SetFillColor(ROOT.kRed+2)
                eff.SetFillStyle(1001)
                lg.AddEntry(eff,'EWK no DY MC','LF')
            elif len(entry)==6:
                draw = '2'
                eff.SetLineColor(ROOT.kRed+2)
                eff.SetFillColor(ROOT.kRed+2)
                eff.SetFillStyle(1001)
                lg.AddEntry(eff,'EWK','LF')
            elif len(entry)==len(use_samples):
                draw = '2'
                eff.SetLineColor(ROOT.kGreen+2)
                eff.SetFillColor(ROOT.kGreen+2)
                eff.SetFillStyle(1001)
                lg.AddEntry(eff,'All Simulation','LF')
            else:
                draw = '2'
                eff.SetLineColor(ROOT.kBlue+2)
                eff.SetFillColor(ROOT.kBlue+2)
                eff.SetFillStyle(1001)
                lg.AddEntry(eff,'EWK+QCD MC','LF')
        #lg.AddEntry(eff, pretty.get(entry.name, entry.name), 'LF')
        draw += same
        eff.Draw(draw)
        effs.append(eff)
        same = ' same'
        outfile.cd()
        eff.Write("arp%d"%iarp)
        iarp+=1
    # end for entry in to_use[name]: # entry is a specific sample


    # t = ROOT.TPaveLabel(0.57, 0.47, 0.90, 0.57, pretty_AliceNoNo, 'brNDC')
    # t.SetTextFont(42)
    # t.SetTextSize(0.4)
    # t.SetTextColor(ROOT.kRed+2)
    # t.SetBorderSize(0)
    # t.SetFillColor(0)
    # t.SetFillStyle(0)
    # tt = ROOT.TPaveLabel(0.57, 0.425, 0.90, 0.525, pretty_AliceNoNo2, 'brNDC')
    # tt.SetTextFont(42)
    # tt.SetTextSize(0.3)
    # tt.SetTextColor(ROOT.kRed+2)
    # tt.SetBorderSize(0)
    # tt.SetFillColor(0)
    # tt.SetFillStyle(0)
    # tt.Draw()
    # t.Draw()

    lg.Draw()
    ps.save(nminus1+'_mass')
    print
    print cartella
# end for name, mass_range in mass_bins: