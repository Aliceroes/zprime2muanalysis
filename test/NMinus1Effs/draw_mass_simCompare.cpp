// Draws comparison plot for simulation of different cuts.
// Takes TGraphAsymmErrors from .root files produced by draw_mass.py
void draw_mass_simCompare()
{
  gROOT -> Reset();

  TCanvas* c1 = new TCanvas("name", "title", 600, 600);
  c1 -> SetBottomMargin(0.2);
  c1 -> SetTopMargin(0.02);
  c1 -> SetRightMargin(0.02);

  gStyle ->SetTitleX(0.25);
  gStyle ->SetTitleY(0.50);
  gStyle->SetOptTitle(0); //this will disable the title for all coming histograms
  gPad -> SetTickx();
  gPad -> SetTicky();

  TFile TrackerIsoRoot("IsoStudies_vsMass/TrackerIso.root","read");
  TFile pfIso03all025Root("IsoStudies_vsMass/pfIso03all025.root","read");
  TFile pfIso03had010Root("IsoStudies_vsMass/pfIso03had010.root","read");

  TGraphAsymmErrors *TrackerIso = new TGraphAsymmErrors();
  TGraphAsymmErrors *pfIso03all025 = new TGraphAsymmErrors();
  TGraphAsymmErrors *pfIso03had010 = new TGraphAsymmErrors();

  TrackerIso = (TGraphAsymmErrors*)TrackerIsoRoot.Get("arp0");
  // TrackerIso -> SetLineColor(kGreen+2);
  // TrackerIso -> SetFillColor(kGreen+2);
  // TrackerIso -> SetFillStyle(1001);
  TrackerIso -> Draw("a2same");

  pfIso03all025 = (TGraphAsymmErrors*)pfIso03all025Root.Get("arp0");
  // pfIso03all025 -> SetLineColor(kGreen+2);
  // pfIso03all025 -> SetFillColor(kGreen+2);
  // pfIso03all025 -> SetFillStyle(1001);
  pfIso03all025 -> Draw("2same");

  pfIso03had010 = (TGraphAsymmErrors*)pfIso03had010Root.Get("arp0");
  // pfIso03had010 -> SetLineColor(kGreen+2);
  // pfIso03had010 -> SetFillColor(kGreen+2);
  // pfIso03had010 -> SetFillStyle(1001);
  pfIso03had010 -> Draw("2same");

  TPaveText *t = new TPaveText(0.25,0.45,0.48,0.5,"blNDC");
  t->AddText("Rel. Iso.");
  t->SetBorderSize(1);
  t->SetFillStyle(0);

  TLegend *lgborder = new TLegend(0.25, 0.21, 0.91, 0.44);

  TLegend *lg = new TLegend(0.265, 0.24, 0.90, 0.41);
  lg -> SetTextSize(0.03);
  lg -> SetTextFont(62);
  lg -> SetFillColor(0);
  lg -> SetBorderSize(0);
  lg -> AddEntry(pfIso03all025, "pf #DeltaR=0.3 < 0.25",  "F");
  lg -> AddEntry(pfIso03had010, "pf #DeltaR=0.3 < 0.10 (charged hadrons)",  "F");
  lg -> AddEntry(TrackerIso, "tracker #DeltaR=0.3 < 0.10",  "F");

  lgborder->Draw();
  lg -> Draw();
  t->Draw();
}
