#!/usr/bin/env python

miniAOD = True

import sys, os, FWCore.ParameterSet.Config as cms
from SUSYBSMAnalysis.Zprime2muAnalysis.Zprime2muAnalysis_cfg import process
from SUSYBSMAnalysis.Zprime2muAnalysis.Zprime2muAnalysis_cff import goodDataFiltersMiniAOD
if miniAOD:
    from SUSYBSMAnalysis.Zprime2muAnalysis.Zprime2muAnalysis_cff import electrons_miniAOD
    electrons_miniAOD(process)
    from SUSYBSMAnalysis.Zprime2muAnalysis.HistosFromPAT_cfi import HistosFromPAT_MiniAOD as HistosFromPAT
else:
    from SUSYBSMAnalysis.Zprime2muAnalysis.HistosFromPAT_cfi import HistosFromPAT
from SUSYBSMAnalysis.Zprime2muAnalysis.OurSelectionDec2012_cff import loose_cut, trigger_match, tight_cut, allDimuons
#from SUSYBSMAnalysis.Zprime2muAnalysis.OurSelection2016_cff import loose_cut, trigger_match, tight_cut, allDimuons

#### if you run on data change HLT2 in
### Zprime2muAnalysis_cff
### (bits = cms.InputTag("TriggerResults","","HLT")) #### instead of HLT2
#### METFilterMiniAOD_cfi.py
#### src = cms.InputTag("TriggerResults","","RECO"), #### instead of PAT


readFiles = cms.untracked.vstring()
secFiles = cms.untracked.vstring()
#process.source = cms.Source ("PoolSource",fileNames = readFiles, secondaryFileNames = secFiles)


# PER RUNNARE SUI DATI DI RUN G
# process.source = cms.Source ("PoolSource",
#                              fileNames =  cms.untracked.vstring("/store/data/Run2016G/SingleMuon/MINIAOD/PromptReco-v1/000/278/820/00000/0667AC34-2464-E611-84CE-02163E011979.root"),
#                              secondaryFileNames = secFiles)

# PER RUNNARE SUI DATI DI RUN G RERECO
# process.source = cms.Source ("PoolSource",
#                              fileNames =  cms.untracked.vstring("/store/data/Run2016G/SingleMuon/MINIAOD/23Sep2016-v1/1110000/9039FEA1-B19C-E611-9CD9-7845C4FC361A.root"),
#                              secondaryFileNames = secFiles)

# RERECO RUN E *
# process.source = cms.Source ("PoolSource",
#                              fileNames =  cms.untracked.vstring("/store/data/Run2016E/SingleMuon/MINIAOD/23Sep2016-v1/50000/0230DB91-868D-E611-A532-0025904A96BC.root"),
#                              secondaryFileNames = secFiles)
# RERECO RUN C *
# process.source = cms.Source ("PoolSource",
#                              fileNames =  cms.untracked.vstring("/store/data/Run2016C/SingleMuon/MINIAOD/23Sep2016-v1/70000/001F13A2-7E8D-E611-B910-FA163E782438.root"),
#                              secondaryFileNames = secFiles)
# RERECO RUN B *
# process.source = cms.Source ("PoolSource",
#                              fileNames =  cms.untracked.vstring("/store/data/Run2016B/SingleMuon/MINIAOD/23Sep2016-v3/00000/00AE0629-1F98-E611-921A-008CFA1112CC.root"),
#                              secondaryFileNames = secFiles)
# RERECO RUN D *
# process.source = cms.Source ("PoolSource",
#                              fileNames =  cms.untracked.vstring("/store/data/Run2016D/SingleMuon/MINIAOD/23Sep2016-v1/010000/0201B90C-C79B-E611-8763-00266CFFBF80.root"),
#                              secondaryFileNames = secFiles)
# RERECO RUN F *
# process.source = cms.Source ("PoolSource",
#                              fileNames =  cms.untracked.vstring("/store/data/Run2016F/SingleMuon/MINIAOD/23Sep2016-v1/100000/0A06DDB0-0A8D-E611-9714-0242AC130002.root"),
#                              secondaryFileNames = secFiles)
# PROMPTRECO RUN H
# process.source = cms.Source ("PoolSource",
                            #  fileNames =  cms.untracked.vstring("/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v3/000/284/036/00000/0E02D50E-989F-E611-A962-FA163EE15C80.root"),
                            #  fileNames =  cms.untracked.vstring("/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/209/00000/CC401A44-6682-E611-A6DE-02163E0141D8.root"),
                            #  secondaryFileNames = secFiles)
process.source = cms.Source ("PoolSource",fileNames = readFiles, secondaryFileNames = secFiles)


# DATI RUN H V2
readFiles.extend( [
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/207/00000/C01B8838-6282-E611-9884-02163E01414B.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/209/00000/CC401A44-6682-E611-A6DE-02163E0141D8.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/213/00000/0854267D-6882-E611-B809-02163E0127FF.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/216/00000/FE504D4D-6582-E611-BE54-FA163E9A6ABB.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/222/00000/689EEDCA-6782-E611-AAAE-FA163E51B535.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/227/00000/86F7468D-6B82-E611-A5B8-FA163E823565.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/231/00000/F85CC211-6C82-E611-960C-02163E011F5F.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/242/00000/3EDCB724-6C82-E611-BD83-FA163E4FF519.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/256/00000/861A5D21-6C82-E611-A260-02163E014396.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/265/00000/28861171-6E82-E611-9CAF-02163E0141FA.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/280/00000/008681A1-7382-E611-B223-02163E01190F.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/285/00000/C4937AEF-7882-E611-9189-FA163E852938.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/286/00000/2A031305-7882-E611-A7D2-02163E0142F6.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/382/00000/8A8C3000-F882-E611-80C6-02163E014266.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/393/00000/6299F577-F982-E611-99F7-FA163E5942E4.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/398/00000/12FC20DA-FE82-E611-912F-02163E011DA1.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/403/00000/22A24310-0483-E611-8217-02163E01462D.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/419/00000/E826CF63-0C83-E611-A18F-02163E01447E.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/439/00000/6E5A5DED-0D83-E611-9BE4-02163E0142B1.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/443/00000/10DF5193-1583-E611-A9F7-FA163E0F2C90.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/454/00000/6070062F-1B83-E611-AE50-FA163EFCD261.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/546/00000/D8F47311-EC83-E611-9AAE-02163E011B72.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/565/00000/EA14E5D7-0484-E611-B99F-FA163ED1977C.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/588/00000/3EE56FAB-2784-E611-9DB4-FA163E6734CA.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/589/00000/B4D5BA85-2884-E611-8D4A-02163E0135E3.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/590/00000/621A900F-2C84-E611-B973-02163E014118.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/613/00000/A4E63E61-C784-E611-BF1C-FA163E61FE92.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/613/00000/D6B8549B-CD84-E611-A3A4-02163E014788.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/613/00000/F086FC23-EC84-E611-B5F5-02163E014627.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/616/00000/A688B9EF-0085-E611-A490-02163E014516.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/616/00000/BE7C05B0-0685-E611-8925-02163E01235B.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/616/00000/D82F7F43-FA84-E611-BB99-02163E013733.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/624/00000/5AD7D61F-ED84-E611-AEE9-FA163E128A85.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/626/00000/44B9A831-0685-E611-951D-02163E01444E.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/632/00000/4C3AD1C0-1A85-E611-84EB-02163E014718.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/636/00000/DABBE11E-5185-E611-ACAD-02163E014124.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/637/00000/1815465A-5185-E611-9E8C-02163E011995.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/638/00000/3A6DEE18-7585-E611-8FF0-02163E0142D6.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/639/00000/3A55AB69-8E85-E611-B299-02163E0119BB.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/639/00000/3CE5AA7E-A185-E611-88FB-FA163E11C6A3.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/639/00000/9CA1213B-8485-E611-B4E2-FA163ED47C66.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/639/00000/9E04A28F-B385-E611-9C07-FA163E80A685.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/639/00000/E0B984BA-9585-E611-BDF4-FA163EACE1FA.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/20E90FAE-9D85-E611-9E16-02163E011944.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/20FE8FC7-B285-E611-98AB-02163E014751.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/221AF2CC-B985-E611-A4D4-02163E0144D2.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/30677DC7-E785-E611-BF7B-02163E011846.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/44F115FD-AD85-E611-93B9-FA163EA8436B.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/540C93ED-A185-E611-A6A0-02163E0140F5.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/96E05E99-AA85-E611-8293-FA163E554A6E.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/9CB63A8B-B585-E611-BC9C-FA163EDA17F7.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/AE49B91A-A885-E611-9C78-FA163EBEB0BF.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/641/00000/E4127622-A885-E611-BB3E-02163E0142C9.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/663/00000/BEEC26C1-9585-E611-BB51-02163E014261.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/674/00000/085B7E53-9985-E611-95E0-FA163EF175CA.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/680/00000/1AC1050A-9D85-E611-8F1F-02163E014560.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/686/00000/562A5BD0-D185-E611-AB38-02163E014307.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/686/00000/78455F5B-EC85-E611-82F5-02163E014313.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/689/00000/82860D0F-C785-E611-9386-02163E014185.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/691/00000/083FA06D-E785-E611-8395-02163E014783.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/005A971D-3C86-E611-AD3B-FA163E940FAB.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/006FF041-3B86-E611-968B-02163E01467F.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/00845D5F-4686-E611-9918-02163E0133A5.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/102A8AA1-5386-E611-80D5-02163E013454.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/14B78DED-4086-E611-A9AA-FA163E70DBFC.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/28A25630-4F86-E611-A0AA-FA163EA4A811.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/2A9AE169-5986-E611-982D-FA163EE5CB53.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/2E011C56-4A86-E611-A113-FA163EE3B504.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/2EC414CA-3D86-E611-855A-FA163E331899.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/366B6FB4-3A86-E611-879A-FA163EF568F8.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/36C7B802-6586-E611-96D2-02163E014379.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/3A85A7C2-5886-E611-ACCC-02163E014518.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/3EFD8EE0-5486-E611-A201-FA163EF59724.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/4066704A-3A86-E611-9DB6-02163E01428D.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/440B0399-4B86-E611-81A4-02163E01220E.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/4AAAFE95-4486-E611-973E-02163E01383A.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/4E82877B-4086-E611-8E87-02163E012677.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/4E8924DC-3B86-E611-BB28-FA163E72F1B8.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/50BBCDD2-4986-E611-B73E-02163E0138F6.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/564039B5-4886-E611-A256-02163E012BE5.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/58C8DBF6-3E86-E611-AA61-02163E014592.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/5A60999C-4686-E611-ADD5-02163E01461E.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/5E30814C-4486-E611-9195-02163E0135BC.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/6099A4C8-3E86-E611-B8F0-02163E0140EE.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/66701DA7-3C86-E611-9152-02163E013392.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/685909E3-3886-E611-BA38-FA163EB1773D.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/6E2406E2-3986-E611-B066-02163E014508.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/6E61548F-4C86-E611-86D6-FA163E95BC9F.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/727401A7-5286-E611-906A-02163E0119EE.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/78FB3C57-4386-E611-B19B-02163E013444.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/7A56776B-5B86-E611-BE98-02163E011BD6.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/7A77D80C-4286-E611-8602-02163E0138A8.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/7C7BB96F-4886-E611-BF19-FA163EF59892.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/7E9D7B44-6F86-E611-A3A3-02163E011A1B.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/7ECCABE9-5686-E611-9BAD-02163E012969.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/80EC0DC9-6186-E611-8A6F-FA163EB6364F.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/84692BE6-4B86-E611-85C3-FA163E2B3AA9.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/869C490C-3786-E611-92A0-02163E014453.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/882BD496-4586-E611-B6BB-02163E014421.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/8A683239-5186-E611-9211-FA163E26AEF7.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/8AAE8FEA-4286-E611-8650-02163E01352D.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/8CCFAC88-5086-E611-98A7-FA163EC473B5.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/8EAEF364-4786-E611-8C1B-FA163EAA0193.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/98107CD5-5586-E611-9C55-02163E01449A.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/9A0C4708-4B86-E611-AAAC-02163E01445B.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/9AE59905-5486-E611-BA55-02163E01472A.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/9C494DC0-3986-E611-8AAA-FA163E920FE6.root',
'/store/data/Run2016H/SingleMuon/MINIAOD/PromptReco-v2/000/281/693/00000/9CBCE17D-3886-E611-A2B5-FA163EB241A3.root',
])

# PER RUNNARE SUI MC
# process.source = cms.Source ("PoolSource",fileNames = readFiles, secondaryFileNames = secFiles)


# PER RUNNARE IN LOCALE SUL SAMPLE 50-120
# readFiles.extend( [
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/0C3755F5-173B-E611-8F95-0CC47A1DF7FE.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/0E2E7BBE-163B-E611-9536-0CC47A1DF82E.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/0EFEE4F7-163B-E611-AE9E-0CC47A1E0722.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/18CC02BC-163B-E611-B776-0CC47A1DF616.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/1A6B76DF-153B-E611-BEC5-0CC47A4DEDF8.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/224D8FB8-163B-E611-A01C-0CC47A4DED50.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/24FCA4F2-153B-E611-92D4-0CC47A4DEDF8.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/2A357448-163B-E611-B2F2-002590D0B0D8.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/2EB67346-163B-E611-91AC-0090FAA57B10.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/32F5AEFA-153B-E611-81A8-002590D0B0CE.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/3A341BDD-163B-E611-97DE-002590D0B04E.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/3E0FAE48-163B-E611-A6BA-002590D0AFCA.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/3E6F88EE-173B-E611-BB31-002590D0AF6A.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/400953D9-163B-E611-B253-0CC47A4DEDF8.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/461DA1BB-173B-E611-B44B-0CC47A4DEDF8.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/48804E46-163B-E611-B9B4-00505602110A.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/4C71F6CB-163B-E611-8E00-002590D0B04E.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/54BB7EB5-163B-E611-9DDE-0090FAA57D64.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/5A51B1C7-173B-E611-932C-0CC47A4DEDF8.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/5E22DFF1-173B-E611-8AD5-0CC47A1DF82E.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/60B79BEE-173B-E611-A508-002590D0AF58.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/68F3A4D3-173B-E611-A2C7-0CC47A1DF82E.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/6C472D40-163B-E611-8120-002590D0AF54.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/74E1A0EC-173B-E611-AF31-0090FAA57D64.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/7E40C397-183B-E611-823B-0CC47A1E046A.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/805435B7-163B-E611-9A18-002590D0AF6A.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/80E236EF-173B-E611-81DA-0CC47A4D9A42.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/8469BBEE-173B-E611-A79F-0025907B4EF0.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/849705F1-153B-E611-A4B9-002590D0B04E.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/884B12C7-163B-E611-9310-0CC47A4DEDF8.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/8A557FE0-153B-E611-9A02-0CC47A4DEDF8.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/8EF513F3-153B-E611-9AEA-002590D0B05C.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/960F81DA-173B-E611-B99B-002590D0B0CE.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/968A39ED-173B-E611-AF1D-0090FAA587C4.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/98CB71F1-153B-E611-A551-002590D0B04E.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/9A333CB7-163B-E611-8A37-0CC47A4DEDF8.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/A0AAD0B9-163B-E611-91A8-0090FAA58D04.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/A2CBD744-163B-E611-B749-0090FAA58BF4.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/A8D4A8F9-173B-E611-BBFA-0CC47A4DED50.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/AAAF4CBF-173B-E611-BED4-002590D0B04E.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/B2F25D46-163B-E611-8E9B-002590D0B0C4.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/B60DB845-163B-E611-A611-0090FAA57CE4.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/BA956AF1-173B-E611-8922-0090FAA58C54.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/C23EA1B5-163B-E611-A953-0090FAA57C20.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/C24A2648-163B-E611-A036-002590D0AF84.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/CA7C9148-163B-E611-845F-0CC47A4D99B0.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/CE2F5BEE-173B-E611-B257-0090FAA59864.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/D6D818B8-163B-E611-B5E9-0CC47A4D9A84.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/E074EEB4-163B-E611-B13A-0090FAA573F0.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/E810BDFA-173B-E611-BD2C-0CC47A1DF616.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/EA85C5F3-173B-E611-8D09-0CC47A1E046A.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/EC7C8C9C-183B-E611-8B47-0CC47A4DEDF8.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/F013ECBC-163B-E611-B061-0CC47A1DFE60.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/F4EE1046-163B-E611-9CCA-0090FAA57D64.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/F661E2FE-163B-E611-9269-002590D0B0CE.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/F6C3F901-183B-E611-9B18-0CC47A1E0722.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/FA5A83F1-173B-E611-B73D-002590D0B04E.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/FE447545-163B-E611-AA97-0090FAA584D4.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/18561D43-D23A-E611-AD05-00259090836A.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/327D7A5E-D23A-E611-AD9B-0CC47A1DF7FE.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/4282FF8C-D23A-E611-8CA5-0090FAA57D64.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/42F7C254-D23A-E611-9EB9-20CF3019DF0F.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/4EED7653-D23A-E611-9C7B-0090FAA57FA4.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/7A41C471-D23A-E611-8775-002590D0AFE6.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/7C16BEBE-D23A-E611-9BC4-0025907B4F72.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/8A2A0353-D23A-E611-9F7F-0090FAA58754.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/8C292346-D23A-E611-8DCD-002590D0B06A.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/8E3FEA52-D23A-E611-BBAD-0090FAA584D4.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/9EF977AF-D23A-E611-A98B-0025907B4F30.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/A86C3453-D23A-E611-9366-0090FAA58BF4.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/B466375E-D23A-E611-BDB2-0CC47A1DF7FE.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/BA1D5655-D23A-E611-8130-002590D0B094.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/BCFE2854-D23A-E611-BE4C-0CC47A4DEF00.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/CE01B355-D23A-E611-9910-0CC47A4DEDEE.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/CE1F5252-D23A-E611-AD56-0090FAA57F14.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/D04AF75C-D23A-E611-B964-0CC47A1DF82E.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/E0E0F15C-D23A-E611-B9BA-0CC47A1DF7FE.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/E2BF0A54-D23A-E611-8182-002590D0B022.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/F4066753-D23A-E611-922A-0090FAA57C20.root',
#  '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_50_120/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/FE9A6C55-D23A-E611-8023-0CC47A4D99A4.root'
#  ] );

# PER RUNNARE IN LOCALE SUL SAMPLE 120-200
# readFiles.extend( [
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_120_200/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/90000/18C80393-613A-E611-86DF-0090FAA573E0.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_120_200/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/90000/AA745369-613A-E611-93A7-0025907B4EE6.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_120_200/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/90000/B2BFFF97-603A-E611-90A2-0CC47AB35D34.root'
# ] );

# PER RUNNARE IN LOCALE SUL SAMPLE 200-400
# readFiles.extend( [
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_200_400/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/62949FDD-BE3A-E611-BCC8-008CFA197FAC.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_200_400/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/A88282DF-BE3A-E611-B325-008CFA197A70.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_200_400/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/CA7CDBE2-BE3A-E611-9717-008CFA198258.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_200_400/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/E2A1D58D-BE3A-E611-8815-0242AC130002.root'
#     ] );

# PER RUNNARE IN LOCALE SUL SAMPLE 400-800
# readFiles.extend( [
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_400_800/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/CAB9C442-A73A-E611-9383-5254003DEED4.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_400_800/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/FA8D18A0-A63A-E611-B09E-5254003DEED4.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_400_800/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/10000/FC331993-A63A-E611-9F72-0242AC130003.root'
# ] );

# PER RUNNARE IN LOCALE SUL SAMPLE 800-1400
# readFiles.extend( [
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_800_1400/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/90000/287981CD-B93B-E611-97F5-0025905A60B0.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_800_1400/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/90000/9A5D76FC-D03B-E611-8AA4-0242AC130004.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_800_1400/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/90000/FE85CB3B-BA3B-E611-BC67-0242AC130002.root'
# ] );

# PER RUNNARE IN LOCALE SUL SAMPLE 1400-2300
# readFiles.extend( [
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_1400_2300/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/70000/449D7C8F-C63B-E611-B26C-00304867FD7B.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_1400_2300/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/70000/881A437D-A63B-E611-96C5-0025905745B8.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_1400_2300/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/70000/9639BE37-C63B-E611-ADE5-02163E00F456.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_1400_2300/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/70000/AC05067C-1C3B-E611-9DA6-001E67DFF4F6.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_1400_2300/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/70000/B8A74EF0-383B-E611-B7E4-00259048AC10.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_1400_2300/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/70000/E0D165F0-AB3B-E611-8BF7-2C600CAFEE50.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_1400_2300/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/70000/F2301E48-C63B-E611-BEAF-001E67C7AC24.root'
#     ] );

# PER RUNNARE IN LOCALE SUL SAMPLE 2300-3500
# readFiles.extend( [
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_2300_3500/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/8C0E76AA-D93C-E611-94E2-0CC47A4D76B6.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_2300_3500/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/B449FDAB-D93C-E611-A15B-0025905B85FE.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_2300_3500/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/F23DCBB0-D93C-E611-B5D6-0025905A606A.root'
# ] );

# PER RUNNARE IN LOCALE SUL SAMPLE 3500-4500
# readFiles.extend( [
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_3500_4500/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/9A55A67B-FD3A-E611-AAFE-001E0BC1E34C.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_3500_4500/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/AE072334-FD3A-E611-B1FC-0025905B8586.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_3500_4500/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/80000/C63C643C-FD3A-E611-AD7A-00237DF284E0.root'
# ] );

# PER RUNNARE IN LOCALE SUL SAMPLE 4500-6000
# readFiles.extend( [
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_4500_6000/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/90000/2E8F7144-A63A-E611-9DB8-001D0967DFF3.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_4500_6000/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/90000/4A0CB50D-A63A-E611-A67A-0090FAA58D04.root',
# '/store/mc/RunIISpring16MiniAODv2/ZToMuMu_NNPDF30_13TeV-powheg_M_4500_6000/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/90000/88049D17-A63A-E611-A789-0090FAA573F0.root'
# ] );



secFiles.extend( [
               ] )

process.maxEvents.input = -1
process.GlobalTag.globaltag = '80X_dataRun2_2016SeptRepro_v4'
#process.MessageLogger.cerr.FwkReport.reportEvery = 1 # default 1000

# Define the numerators and denominators, removing cuts from the
# allDimuons maker. "NoX" means remove cut X entirely (i.e. the
# loose_cut denominators), "TiX" means move cut X from the loose_cut
# to the tight_cut (meaning only one muon instead of two has to pass
# the cut).  "NoNo" means remove nothing (i.e. the numerator). This
# will break if loose_, tight_cut strings are changed upstream, so we
# try to check those with a simple string test below.

cuts = [
    ('Pt',      'pt > 53'),
    ('DB',      'abs(dB) < 0.2'),
    ('Iso',     'isolationR03.sumPt / innerTrack.pt < 0.10'),
    ('TkLayers','globalTrack.hitPattern.trackerLayersWithMeasurement > 5'),
    ('PxHits',  'globalTrack.hitPattern.numberOfValidPixelHits >= 1'),
    ('MuHits',  'globalTrack.hitPattern.numberOfValidMuonHits > 0'),
    # OLD CUT
    ('MuMatch', ('numberOfMatchedStations > 1', 'isTrackerMuon')),
    # NEW CUT
    #('MuMatch', ('( numberOfMatchedStations > 1 || (numberOfMatchedStations == 1 && !(stationMask == 1 || stationMask == 16)) || (numberOfMatchedStations == 1 && (stationMask == 1 || stationMask == 16) && numberOfMatchedRPCLayers > 2))', 'isTrackerMuon')),
    ]

for name, cut in cuts:
    if type(cut) != tuple:
        cut = (cut,)

    lc = loose_cut
    for c in cut:
        if c not in lc:
            raise ValueError('cut "%s" not in cut string "%s"' % (c, lc))
        lc = lc.replace(' && ' + c, '') # Relies on none of the cuts above being first in the list.

    obj_no = allDimuons.clone(loose_cut = lc)
    #obj_no = allDimuons.clone(loose_cut = lc,tight_cut = tight_cut.replace(trigger_match, ''))#N-2
    setattr(process, 'allDimuonsNo' + name, obj_no)

    # obj_ti = obj_no.clone(tight_cut = tight_cut + ' && ' + ' && '.join(cut))
    # setattr(process, 'allDimuonsTi' + name, obj_ti)

process.allDimuonsNoNo      = allDimuons.clone()
#process.allDimuonsNoNo      = allDimuons.clone(tight_cut = tight_cut.replace(trigger_match, ''))#N-2
process.allDimuonsNoTrgMtch = allDimuons.clone(tight_cut = tight_cut.replace(trigger_match, ''))

alldimus = [x for x in dir(process) if 'allDimuonsNo' in x or 'allDimuonsTi' in x]

# Sanity check that the replaces above did something.
for x in alldimus:
    if 'NoNo' in x:
        continue
    o = getattr(process, x)
    assert o.loose_cut.value() != loose_cut or o.tight_cut.value() != tight_cut

if miniAOD:
    process.load('SUSYBSMAnalysis.Zprime2muAnalysis.DileptonPreselector_cfi')####?????
    process.leptons = process.leptonsMini.clone()
    process.p = cms.Path(process.egmGsfElectronIDSequence*process.dileptonPreseletor * process.muonPhotonMatchMiniAOD * process.leptons * reduce(lambda x,y: x*y, [getattr(process, x) for x in alldimus]))
    process.load('SUSYBSMAnalysis.Zprime2muAnalysis.goodData_cff')
    for dataFilter in goodDataFiltersMiniAOD:
        #setattr(process,dataFilter
        process.p *= dataFilter
else:
    process.leptons = process.leptons.clone()
    process.p = cms.Path(process.goodDataFilter * process.muonPhotonMatch * process.leptons * reduce(lambda x,y: x*y, [getattr(process, x) for x in alldimus]))



# For all the allDimuons producers, make dimuons producers, and
# analyzers to make the histograms.
for alld in alldimus:
    dimu = process.dimuons.clone(src = alld)
    name = alld.replace('allD', 'd')
    setattr(process, name, dimu)
    hists = HistosFromPAT.clone(dilepton_src = name, leptonsFromDileptons = True)
    setattr(process, name.replace('dimuons', ''), hists)
    process.p *= dimu * hists

# Handle the cuts that have to be applied at the
# Zprime2muCompositeCandidatePicker level.
#process.allDimuonsN2 = allDimuons.clone(tight_cut = tight_cut.replace(trigger_match, ''))#N-2
#process.p *= process.allDimuonsN2#N-2
process.dimuonsNoB2B     = process.dimuons.clone()
process.dimuonsNoVtxProb = process.dimuons.clone()
process.dimuonsNoDptPt   = process.dimuons.clone()
#process.dimuonsNoB2B     = process.dimuons.clone(src = 'allDimuonsN2')#N-2
#process.dimuonsNoVtxProb = process.dimuons.clone(src = 'allDimuonsN2')#N-2
#process.dimuonsNoDptPt   = process.dimuons.clone(src = 'allDimuonsN2')#N-2
delattr(process.dimuonsNoB2B,     'back_to_back_cos_angle_min')
delattr(process.dimuonsNoVtxProb, 'vertex_chi2_max')
delattr(process.dimuonsNoDptPt,   'dpt_over_pt_max')

process.p *= process.allDimuons
for dimu in ['dimuonsNoB2B', 'dimuonsNoVtxProb', 'dimuonsNoDptPt']:
    hists = HistosFromPAT.clone(dilepton_src = dimu, leptonsFromDileptons = True)
    setattr(process, dimu.replace('dimuons', ''), hists)
    process.p *= getattr(process, dimu) * hists

# Special case to remove |dB| and B2B cuts simultaneously, as they can
# be correlated (anti-cosmics).
process.allDimuonsNoCosm = process.allDimuons.clone(loose_cut = loose_cut.replace(' && abs(dB) < 0.2', ''))
#process.allDimuonsNoCosm = process.allDimuons.clone(loose_cut = loose_cut.replace(' && abs(dB) < 0.2', ''), tight_cut = tight_cut.replace(trigger_match, '')) #N-2
process.dimuonsNoCosm = process.dimuons.clone(src = 'allDimuonsNoCosm')
delattr(process.dimuonsNoCosm, 'back_to_back_cos_angle_min')
process.NoCosm = HistosFromPAT.clone(dilepton_src = 'dimuonsNoCosm', leptonsFromDileptons = True)
process.p *= process.allDimuonsNoCosm * process.dimuonsNoCosm * process.NoCosm

print "CONTROLLA IL NUMERO DI EVENTI DA PROCESSARE\nNe stai processando: ", process.maxEvents.input

f = file('outfile', 'w')
f.write(process.dumpPython())
f.close()
if __name__ == '__main__' and 'submit' in sys.argv:
    crab_cfg = '''
from CRABClient.UserUtilities import config
config = config()
config.General.requestName = 'ana_nminus1_%(name)s'
config.General.workArea = 'crab'
#config.General.transferLogs = True
config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'nminus1effs.py'
#config.JobType.priority = 1
#config.Data.inputDataset =  '%(ana_dataset)s' #for pattuples
config.Data.inputDataset =  '%(dataset)s' # for miniAOD
config.Data.inputDBS = 'global'
job_control
config.Data.publication = False
config.Data.outputDatasetTag = 'ana_datamc_%(name)s'
config.Data.outLFNDirBase = '/store/user/aalfonsi'
#config.Site.storageSite = 'T2_IT_Bari'
config.Site.storageSite = 'T2_IT_Legnaro'
'''

    just_testing = 'testing' in sys.argv
    if not 'no_data' in sys.argv:
        #running on miniaod we don't need of googlumis as it was
        #from SUSYBSMAnalysis.Zprime2muAnalysis.goodlumis import Run2016G_ll
        #Run2016G_ll.writeJSON('tmp.json')

        dataset_details = [
        # ('SingleMuonRun2016GPrompt_NoCracks',  '/SingleMuon/Run2016G-PromptReco-v1/MINIAOD')
        # RERECO
        # ('SingleMuonRun2016GReReco_NoCracks', '/SingleMuon/Run2016G-23Sep2016-v1/MINIAOD')
        # ('SingleMuonRun2016F-23Sep2016-v1_NoCracks', '/SingleMuon/Run2016F-23Sep2016-v1/MINIAOD'),
        # ('SingleMuonRun2016E-23Sep2016-v1_NoCracks', '/SingleMuon/Run2016E-23Sep2016-v1/MINIAOD'),
        # ('SingleMuonRun2016C-23Sep2016-v1_NoCracks', '/SingleMuon/Run2016C-23Sep2016-v1/MINIAOD'),
        # ('SingleMuonRun2016B-23Sep2016-v3_NoCracksTRY2', '/SingleMuon/Run2016B-23Sep2016-v3/MINIAOD'),
        # ('SingleMuonRun2016D-23Sep2016-v1_NoCracks', '/SingleMuon/Run2016D-23Sep2016-v1/MINIAOD'),
        # ('SingleMuonRun2016H-PromptReco-v3_NoCracks', '/SingleMuon/Run2016H-PromptReco-v3/MINIAOD'),
        ('SingleMuonRun2016H-PromptReco-v2', '/SingleMuon/Run2016H-PromptReco-v2/MINIAOD'),
        ]

    #    for name, ana_dataset in dataset_details: # for pattuples
        for name, dataset in dataset_details: # for miniAOD

            print name

            new_py = open('nminus1effs.py').read()
            # RUN G PROMPT RECO
            # new_py += "\nprocess.GlobalTag.globaltag = '80X_dataRun2_Prompt_v11'\n"
            # RUN C-F e G RERECO
            # new_py += "\nprocess.GlobalTag.globaltag = '80X_dataRun2_2016SeptRepro_v3'\n"
            # RUN B RERECO
            # new_py += "\nprocess.GlobalTag.globaltag = '80X_dataRun2_2016SeptRepro_v4'\n"
            # RUN H PROMPTRECO
            new_py += "\nprocess.GlobalTag.globaltag = '80X_dataRun2_Prompt_v14'\n"
            open('nminus1effs_crab.py', 'wt').write(new_py)

            new_crab_cfg = crab_cfg % locals()
            job_control = '''
config.Data.splitting = 'LumiBased'
config.Data.totalUnits = -1
config.Data.unitsPerJob = 100 # si usa 100 in genere per i dati
#config.Data.lumiMask = 'tmp.json' #######
config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Cert_271036-282037_13TeV_PromptReco_Collisions16_JSON_NoL1T_MuonPhys.txt'
'''
            new_crab_cfg = new_crab_cfg.replace('job_control', job_control)
            open('crabConfig.py', 'wt').write(new_crab_cfg)

            if not just_testing:
                os.system('crab submit -c crabConfig.py ') #--dryrun

        if not just_testing:
            os.system('rm crabConfig.py nminus1effs_crab.py nminus1effs_crab.pyc tmp.json')

    if not 'no_mc' in sys.argv:
        crab_cfg = crab_cfg.replace('job_control','''
config.Data.splitting = 'EventAwareLumiBased'
config.Data.totalUnits = -1
config.Data.unitsPerJob  = 10000
''')

        from SUSYBSMAnalysis.Zprime2muAnalysis.MCSamples import *
        samples =[DY50to120Powheg,DY120to200Powheg,DY200to400Powheg,DY400to800Powheg,DY800to1400Powheg,DY1400to2300Powheg,DY2300to3500Powheg,DY3500to4500Powheg,DY4500to6000Powheg,DY6000toInfPowheg]#,ttbar, wz, ww_incl, zz_incl, dy50to120]
        # samples =[
                #   dy50to120, dy120to200, dy200to400, dy400to800, dy800to1400, dy1400to2300,dy2300to3500, dy3500to4500, dy4500to6000,
                  #  WZ, ZZ, WW200to600, WW600to1200, WW1200to2500, WW2500,
                  #Wjets, ttbar_lep,
                  #                Wantitop, tW,
                  #                qcd80to120, qcd120to170, qcd170to300, qcd300to470, qcd470to600, qcd600to800, qcd800to1000, qcd1000to1400, qcd1400to1800, qcd1800to2400, qcd2400to3200, qcd3200
                  ]
        for sample in samples:
            #print sample.name
            open('crabConfig.py', 'wt').write(crab_cfg % sample)
            if not just_testing:
                os.system('crab submit -c crabConfig.py')
                #os.system('crab submit -c crabConfig.py --dryrun')
        if not just_testing:
            os.system('rm crabConfig.py')
