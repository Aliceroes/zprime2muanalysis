void draw_RunGCompare_Neffs()
{
  // DATI: arp1 (60-120), arp3 (>120)
  // DY: arp0 (60-120), arp2 (>120)
  gROOT -> Reset();

  TCanvas* c1 = new TCanvas("name", "title", 599, 600);
  c1 -> SetBottomMargin(0.21);
  c1 -> SetTopMargin(0.02);
  c1 -> SetRightMargin(0.02);

  gStyle ->SetTitleX(0.25);
  gStyle ->SetTitleY(0.50);
  gStyle->SetOptTitle(0); //this will disable the title for all coming histograms
  gPad -> SetTickx();
  gPad -> SetTicky();

  TFile PromptRecoRoot("RUNGPromptVSRe/RunGPromptReco.root","read");
  TFile CFReRecoRoot("plots/RunC-FReReco.root","read");
  TFile ReRecoRoot("RUNGPromptVSRe/RunGReReco.root","read");

  TGraphAsymmErrors *PromptReco = new TGraphAsymmErrors();
  TGraphAsymmErrors *ReReco = new TGraphAsymmErrors();
  TGraphAsymmErrors *CFReReco = new TGraphAsymmErrors();
  TGraphAsymmErrors *DY = new TGraphAsymmErrors();

  // PromptReco = (TGraphAsymmErrors*)PromptRecoRoot.Get("arp3");
  // PromptReco -> SetLineColor(kRed+1);
  // PromptReco -> SetMarkerColor(kRed+1);
  // PromptReco -> SetFillColor(kRed+1);
  // PromptReco -> SetMarkerSize(1.1);
  // // PromptReco -> SetFillStyle(1001);
  // PromptReco -> Draw("A0EP");

  CFReReco = (TGraphAsymmErrors*)CFReRecoRoot.Get("arp3");
  CFReReco -> SetLineColor(kGreen+2);
  CFReReco -> SetFillColor(kGreen+2);
  CFReReco -> SetMarkerColor(kGreen+2);
  CFReReco -> SetMarkerSize(1.1);
  // ReReco -> SetFillStyle(1001);
  CFReReco -> Draw("AsameE0P");

  ReReco = (TGraphAsymmErrors*)ReRecoRoot.Get("arp3");
  ReReco -> SetLineColor(kBlue+1);
  ReReco -> SetFillColor(kBlue+1);
  ReReco -> SetMarkerColor(kBlue+1);
  ReReco -> SetMarkerSize(1.1);
  // ReReco -> SetFillStyle(1001);
  ReReco -> Draw("sameE0P");

  // DY = (TGraphAsymmErrors*)ReRecoRoot.Get("arp2");
  // pfIso03had010 -> SetLineColor(kGreen+2);
  // pfIso03had010 -> SetFillColor(kGreen+2);
  // pfIso03had010 -> SetFillStyle(1001);
  // DY -> Draw("2same");

  TPaveText *t = new TPaveText(0.25,0.45,0.57,0.5,"blNDC");
  // TPaveText *t = new TPaveText(0.25,0.45,0.48,0.5,"blNDC");
  // t->SetTextFont(63);
  // t->SetTextSize(20);
  // t->AddText("60 < m < 120 GeV"); // DATI arp1, DY arp0
  t->AddText("m > 120 GeV"); // DATI arp3, DY arp2
  t->SetBorderSize(1);
  t->SetFillStyle(0);

  TLegend *lgborder = new TLegend(0.25, 0.21, 0.94, 0.44);

  TLegend *lg = new TLegend(0.265, 0.24, 0.90, 0.41);
  lg -> SetTextSize(0.03);
  lg -> SetTextFont(62);
  lg -> SetFillColor(0);
  lg -> SetBorderSize(0);
  // lg -> AddEntry(PromptReco, "RunG PromptReco, 7.6 fb^{-1}, MuonOnly",  "Lp");
  lg -> AddEntry(CFReReco, "RunC-F ReReco, 14.3 fb^{-1}, MuonOnly",  "Lp");
  lg -> AddEntry(ReReco, "RunG ReReco, 7.6 fb^{-1}, MuonOnly",  "Lp");
  // lg -> AddEntry(DY, "DY MC 80X",  "F");

  lgborder->Draw();
  lg -> Draw();
  t->Draw();
}
