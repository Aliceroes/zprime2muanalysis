void draw_mass_CracksCompare()
{
  //  arp0 = DY, arp1 = data
  gROOT -> Reset();

  TCanvas* c1 = new TCanvas("name", "title", 600, 600);
  c1 -> SetBottomMargin(0.2);
  c1 -> SetTopMargin(0.02);
  c1 -> SetRightMargin(0.04);

  gStyle ->SetTitleX(0.25);
  gStyle ->SetTitleY(0.50);
  gStyle->SetOptTitle(0); //this will disable the title for all coming histograms
  gPad -> SetTickx();
  gPad -> SetTicky();

  TFile CracksRoot("plots/MatchedStations/PromptRecoWithCracks.root","read");
  TFile NoCracksRoot("plots/MatchedStations/PromptRecoNoCracks.root","read");

  TGraphAsymmErrors *Cracks = new TGraphAsymmErrors();
  TGraphAsymmErrors *NoCracks = new TGraphAsymmErrors();
  TGraphAsymmErrors *DYCracks = new TGraphAsymmErrors();
  TGraphAsymmErrors *DYNoCracks = new TGraphAsymmErrors();


  // DY reHLT

  DYCracks = (TGraphAsymmErrors*)CracksRoot.Get("arp0");
  DYCracks -> SetLineColor(kRed+1);
  DYCracks -> SetFillColor(kRed+1);
  DYCracks -> SetFillStyle(1001);
  DYCracks -> Draw("a2same");

  DYNoCracks = (TGraphAsymmErrors*)NoCracksRoot.Get("arp0");
  DYNoCracks -> SetLineColor(kGreen+2);
  DYNoCracks -> SetFillColor(kGreen+2);
  DYNoCracks -> SetFillStyle(1001);
  DYNoCracks -> Draw("2same");


  // DATA RUN G
  //
  // Cracks = (TGraphAsymmErrors*)CracksRoot.Get("arp1");
  // Cracks -> SetLineColor(kRed+1);
  // Cracks -> SetMarkerColor(kRed+1);
  // Cracks -> SetFillColor(kRed+1);
  // Cracks -> SetMarkerSize(1.1);
  // Cracks -> Draw("A0EP");
  //
  // NoCracks = (TGraphAsymmErrors*)NoCracksRoot.Get("arp1");
  // NoCracks -> SetLineColor(kGreen+2);
  // NoCracks -> SetMarkerColor(kGreen+2);
  // NoCracks -> SetFillColor(kGreen+2);
  // NoCracks -> SetMarkerSize(1.1);
  // NoCracks -> Draw("0EPsame");


  // LEGEND

  // DATA
  TPaveText *t = new TPaveText(0.15,0.45,0.51,0.5,"blNDC");
  //  reHLT DY
  // TPaveText *t = new TPaveText(0.25,0.45,0.48,0.5,"blNDC");
  // t->AddText("RunG ReReco, 7.6 fb^{-1}, MuonOnly");
  // t->AddText("RunG PromptReco, 7.6 fb^{-1}, MuonOnly");
  t->AddText("DY MC 80X");
  t->SetBorderSize(1);
  t->SetFillStyle(0);

  TLegend *lgborder = new TLegend(0.15, 0.21, 0.51, 0.44);

  TLegend *lg = new TLegend(0.165, 0.24, 0.50, 0.41);
  lg -> SetTextSize(0.03);
  lg -> SetTextFont(62);
  lg -> SetFillColor(0);
  lg -> SetBorderSize(0);
  // DATA
  // lg -> AddEntry(Cracks, "With cracks",  "LP");
  // lg -> AddEntry(NoCracks, "Cracks removed",  "LP");
  // DY reHLT
  lg -> AddEntry(DYCracks, "With cracks",  "F");
  lg -> AddEntry(DYNoCracks, "Cracks removed",  "F");

  lgborder->Draw();
  lg -> Draw();
  t->Draw();
}
